# Jira Macro Keyboard #

This is a QMK Keyboard firmware for the Sweet 16 macro keyboard to make 16 Jira hotkeys available in a special peripheral

![](jiramacrokeyboard.png)


### How do I get set up? ###

* You need the Arduino IDE or other suitable loader for the Arduino Pro Micro
* Try the [QMK Toolbox](https://github.com/qmk/qmk_toolbox/releases)

### What are the keys?

* G: Go to a...

* D: Dashboard

* P: Project 

* I: Issue

* .: Quick Operations

* E: Edit Issue

* A: Assign Issue

* U: Search for Issues 

* O: View selected Issue

* C: Create an Issue

* ,: Jump fields

* J: Next Issue

* <-: Previous Field

* ->: Next Field

* K: Previous Issue
